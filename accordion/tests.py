from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse

import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import index

# Create your tests here.

class UrlsTest(TestCase):

    def setUp(self):
        self.index = reverse("accordion:index")
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.index = reverse("accordion:index")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "accordion/index.html")

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_title(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        self.assertEqual(selenium.title, "My Accordion")

    def test_element_expand(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # find all accordions head and body(value)
        accordion1 = selenium.find_element_by_css_selector("#accor1 > div.header.d-flex.justify-content-between.align-items-center > h2")
        accordion1Content = selenium.find_element_by_css_selector("#accor1 > div.content")
        accordion2 = selenium.find_element_by_css_selector("#accor2 > div.header.d-flex.justify-content-between.align-items-center > h2")
        accordion2Content = selenium.find_element_by_css_selector("#accor2 > div.content")
        accordion3 = selenium.find_element_by_css_selector("#accor3 > div.header.d-flex.justify-content-between.align-items-center > h2")
        accordion3Content = selenium.find_element_by_css_selector("#accor3 > div.content")
        accordion4 = selenium.find_element_by_css_selector("#accor4 > div.header.d-flex.justify-content-between.align-items-center > h2")
        accordion4Content = selenium.find_element_by_css_selector("#accor4 > div.content")
        accordion5 = selenium.find_element_by_css_selector("#accor5 > div.header.d-flex.justify-content-between.align-items-center > h2")
        accordion5Content = selenium.find_element_by_css_selector("#accor5 > div.content")
        # the body(value) should hidden in the first place
        self.assertEqual("none", accordion1Content.value_of_css_property("display"))
        self.assertEqual("none", accordion2Content.value_of_css_property("display"))
        self.assertEqual("none", accordion3Content.value_of_css_property("display"))
        self.assertEqual("none", accordion4Content.value_of_css_property("display"))
        self.assertEqual("none", accordion5Content.value_of_css_property("display"))
        # clicking each element header
        accordion1.click()
        time.sleep(1)
        accordion2.click()
        time.sleep(1)
        accordion3.click()
        time.sleep(1)
        accordion4.click()
        time.sleep(1)
        accordion5.click()
        time.sleep(1)
        # after clicking the element the body(value) should be displayed block
        self.assertEqual("block", accordion1Content.value_of_css_property("display")) 
        self.assertEqual("block", accordion2Content.value_of_css_property("display")) 
        self.assertEqual("block", accordion3Content.value_of_css_property("display")) 
        self.assertEqual("block", accordion4Content.value_of_css_property("display")) 
        self.assertEqual("block", accordion5Content.value_of_css_property("display")) 
        # clicking again the header
        accordion1.click()
        time.sleep(1)
        accordion2.click()
        time.sleep(1)
        accordion3.click()
        time.sleep(1)
        accordion4.click()
        time.sleep(1)
        accordion5.click()
        time.sleep(1)
        # now the accordion body should be hidden again
        self.assertEqual("none", accordion1Content.value_of_css_property("display")) 
        self.assertEqual("none", accordion2Content.value_of_css_property("display")) 
        self.assertEqual("none", accordion3Content.value_of_css_property("display")) 
        self.assertEqual("none", accordion4Content.value_of_css_property("display")) 
        self.assertEqual("none", accordion5Content.value_of_css_property("display")) 



    def test_up_button(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # get the accordion 2
        accordion2 = selenium.find_element_by_id("accor2")
        # get the up button from accordion 2
        accordion2Up = selenium.find_element_by_css_selector("#accor2 > div.header.d-flex.justify-content-between.align-items-center > div > div.up.p-2.d-flex.align-items-center.justify-content-center.col-6")
        # before, order of accordion 2 should be 2
        self.assertEqual("2", accordion2.value_of_css_property("order"))
        # click the up button on accordion 2
        accordion2Up.click()
        # now the order of accordion 2 should be 1
        self.assertEqual("1", accordion2.value_of_css_property("order"))

    def test_down_button(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # get the accordion 3
        accordion2 = selenium.find_element_by_id("accor3")
        # get the down button from accordion 3
        accordion2down = selenium.find_element_by_css_selector("#accor3 > div.header.d-flex.justify-content-between.align-items-center > div > div.down.p-2.d-flex.align-items-center.justify-content-center.col-6")
        # before, order of accordion 3 should be 3
        self.assertEqual("3", accordion2.value_of_css_property("order"))
        # click the down button on accordion 3
        accordion2down.click()
        # now the order of accordion 3 should be 4
        self.assertEqual("4", accordion2.value_of_css_property("order"))

    def test_change_theme(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # find the button to change theme
        button = selenium.find_element_by_css_selector("label.switch")
        # find elemen which backgroud color will change
        wrapper = selenium.find_element_by_css_selector(".wrapper")
        header = selenium.find_element_by_css_selector(".header")
        up = selenium.find_element_by_css_selector(".up")
        down = selenium.find_element_by_css_selector(".down")
        content = selenium.find_element_by_css_selector(".content")
        # check initial css before click
        self.assertEqual("rgba(255, 255, 255, 1)", wrapper.value_of_css_property("background-color"))
        self.assertEqual("rgba(176, 234, 205, 1)", header.value_of_css_property("background-color"))
        self.assertEqual("rgba(33, 191, 115, 1)", up.value_of_css_property("background-color"))
        self.assertEqual("rgba(253, 94, 83, 1)", down.value_of_css_property("background-color"))
        self.assertEqual("rgba(249, 252, 251, 1)", content.value_of_css_property("background-color"))
        # click the button
        button.click()
        time.sleep(2)
        # check css after click
        self.assertEqual("rgba(32, 32, 64, 1)", wrapper.value_of_css_property("background-color"))
        self.assertEqual("rgba(176, 48, 176, 1)", header.value_of_css_property("background-color"))
        self.assertEqual("rgba(96, 32, 128, 1)", up.value_of_css_property("background-color"))
        self.assertEqual("rgba(32, 32, 96, 1)", down.value_of_css_property("background-color"))
        self.assertEqual("rgba(51, 49, 59, 1)", content.value_of_css_property("background-color"))
        # click the button again
        button.click()
        time.sleep(2)
        # back to initial css
        self.assertEqual("rgba(255, 255, 255, 1)", wrapper.value_of_css_property("background-color"))
        self.assertEqual("rgba(176, 234, 205, 1)", header.value_of_css_property("background-color"))
        self.assertEqual("rgba(33, 191, 115, 1)", up.value_of_css_property("background-color"))
        self.assertEqual("rgba(253, 94, 83, 1)", down.value_of_css_property("background-color"))
        self.assertEqual("rgba(249, 252, 251, 1)", content.value_of_css_property("background-color"))

