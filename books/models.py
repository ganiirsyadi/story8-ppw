from django.db import models

# Create your models here.

class Book(models.Model):
    book_id = models.CharField(max_length=25)
    book_title = models.CharField(max_length=200)
    book_img = models.CharField(max_length=250)
    book_link = models.CharField(max_length=200)
    count_like = models.PositiveIntegerField()
    def __str__(self):
        return self.book_title