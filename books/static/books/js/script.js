$('.search-button').click(() => getData())

$('.input-keyword').keypress((e) => {
    if (e.which == 13) {
        getData()
    }
})

$(document).on("click", ".like", function () {
    let book_data = $(this).data("book").split("#^%|^#")
    let book_id = book_data[0]
    let book_link = book_data[1]
    let book_title = book_data[2]
    let book_img = book_data[3]
    let currentElement = $(this)
    let currentLike = $(this).next().children().eq(0).html()
    $(this).next().children().eq(0).html(1 + parseInt(currentLike))
    $.post($("meta[name='index']").attr("content") + "add/", {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
        book_id : book_id,
        book_title : book_title,
        book_img : book_img,
        book_link : book_link
    })
    .fail(() => {
        $.get($("meta[name='index']").attr("content") + 'count_like/' + book_id)
        .done(e => currentElement.next().children().eq(0).html(e.likes))
    })
})

$(document).on("click", ".btn-modal", function () {
    $.get($("meta[name='index']").attr("content") + 'top5/')
    .done((e) => {
        $(".modal-body").empty()
        if (e.length > 0) {
            e.forEach((book) => {
                $(".modal-body").append(getModal(book.fields))
            })
        } else {
            $(".modal-body").html('<h5 class="text-center">Discover more books and like your fav!</h5>')
        }
    })
})

// Functions

function generateRow(b, count_like) {
    let link = b.volumeInfo.infoLink
    let desc = b.volumeInfo.description != undefined ? b.volumeInfo.description : "No Description"
    let thumbnail = b.volumeInfo.imageLinks
    let imgCover = thumbnail != null ? thumbnail.smallThumbnail.toString() : $("meta[name='images']").attr("content") + "book-placeholder.png"; 
    let imgHeart = $("meta[name='images']").attr("content") + "heart.svg"
    let dataArray = [b.id, link, b.volumeInfo.title, imgCover].join("#^%|^#")
    return `<div class="box d-flex align-items-start p-2 border-top border-bottom">
                <div class="cover d-flex justify-content-center">
                    <img src="${imgCover}" alt="">
                </div>
                <div class="d-flex flex-column ml-1 ml-md-3 col-md-12 col-10">
                    <a href="${link}" class="text-dark" target="_blank">
                    <h3 class="text-truncate col-9 p-0 title">${b.volumeInfo.title}</h3></a>
                    <p class="desc mb-1 col-10 p-0">${desc}</p>
                    <div class="d-flex justify-content-between col-10 pl-0">
                    <button class="like btn btn-outline-dark" type="button" data-book="${dataArray}">Like</button> 
                    <div class="d-flex align-items-center">
                        <h5 class="mr-2 count-like">${count_like}</h5>
                        <img src="${imgHeart}" width=20>
                    </div>
                    </div>
                </div>
            </div>`
}

function getData() { 
    $.get("https://www.googleapis.com/books/v1/volumes?q=" + $('.input-keyword').val())
    .done((results) => {
        $(".content").empty()
        let content = $(".content")
        if (results.totalItems == 0) {
            content.append("<h5>Does your book come from another dimension?</h5>")
        } else {
            let books = results.items
            let first = "<h4>Search Result:</h4>"
            content.append(first)
            books.forEach(b => {
                $.get($("meta[name='index']").attr("content") + 'count_like/' + b.id)
                .done(e => content.append(generateRow(b, e.likes)))
            })
        }
    }) 
}

function getModal(book) {
    let imgHeart = $("meta[name='images']").attr("content") + "heart.svg"
    return `<div class="box d-flex align-items-center p-2 border-top border-bottom">
    <img class="cover" src="${ book.book_img }" alt="">
    <div class="d-flex flex-column justify-content-center ml-1 ml-md-3 col-md-9 col-8">
      <h3 class="text-truncate p-0 title">${ book.book_title }</h3>
      <a href="${book.book_link}" target="_blank" class="mb-4">Read more...</a>
      <div class="d-flex align-items-center">
        <h5 class="mr-2 mb-1 count-like">${ book.count_like }</h5>
        <img src="${ imgHeart }" width=20>
      </div>
    </div>
  </div>`
}