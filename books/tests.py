from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Book
from .views import *
import json
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options



# Create your tests here.

class ModelTest(TestCase):

    def setUp(self):
        self.book = Book.objects.create(
            book_id = "Y1lYzQEACAAJ",
            book_title = "Gani",
            book_img = "http://books.google.com/books/content?id=Y1lYzQEACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
            book_link = "https://www.googleapis.com/books/v1/volumes/Y1lYzQEACAAJ",
            count_like = 1
        )

    def test_instance_created(self):
        self.assertEqual(Book.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.book), "Gani")

class UrlsTest(TestCase):

    def setUp(self):
        self.book = Book.objects.create(
            book_id = "123",
            book_title = "Apple",
            book_img = "http://books.google.com/books/content?id=Y1lYzQEACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
            book_link = "https://www.googleapis.com/books/v1/volumes/Y1lYzQEACAAJ",
            count_like = 1
        )
        self.index = reverse("books:index")
        self.add = reverse("books:add")
        self.count_like = reverse("books:count_like", args=[self.book.book_id])
        self.get_top5 = reverse("books:get_top5")
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_add_use_right_function(self):
        found = resolve(self.add)
        self.assertEqual(found.func, add)
    
    def test_count_like_use_right_function(self):
        found = resolve(self.count_like)
        self.assertEqual(found.func, count_like)

    def test_get_top5_use_right_function(self):
        found = resolve(self.get_top5)
        self.assertEqual(found.func, get_top5)

class ViewsTest(TestCase):

    def setUp(self):
        self.book = Book.objects.create(
            book_id = "456",
            book_title = "Kopi",
            book_img = "http://books.google.com/books/content?id=Y1lYzQEACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
            book_link = "https://www.googleapis.com/books/v1/volumes/Y1lYzQEACAAJ",
            count_like = 1
        )

        self.index = reverse("books:index")
        self.add = reverse("books:add")
        self.count_like_exist = reverse("books:count_like", args=[self.book.book_id])
        self.count_like_not_exist = reverse("books:count_like", args=['08123456'])
        self.get_top5 = reverse("books:get_top5")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "books/index.html")

    def test_POST_add(self):
        # if the book doesn't exist at the db
        # it should add it to the db
        response = self.client.post(self.add, 
        {
            'book_id': '2429',
            'book_title': 'Bucin',
            'book_img': 'images.google.com',
            'book_link': 'www.bucinology.com'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        newBook = Book.objects.filter(book_id='2429')
        self.assertEqual(len(newBook), 1)

        # if the book already exist
        # it increaments the count_like by 1
        response = self.client.post(self.add, 
        {
            'book_id': '2429',
            'book_title': 'Bucin',
            'book_img': 'images.google.com',
            'book_link': 'www.bucinology.com'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        newBook = Book.objects.get(book_id='2429')
        self.assertEqual(newBook.count_like, 2)

    def test_GET_count_like(self):
        response_exist = self.client.get(self.count_like_exist)
        self.assertEqual(response_exist.status_code, 200)
        self.assertJSONEqual(response_exist.content, {'likes' : 1})
        response_not_exist = self.client.get(self.count_like_not_exist)
        self.assertEqual(response_not_exist.status_code, 200)
        self.assertJSONEqual(response_not_exist.content, {'likes' : 0})

    def test_GET_get_top5(self):
        response = self.client.get(self.get_top5)
        self.assertEqual(response.status_code, 200)
        self.assertEquals(len(json.loads(response.content)),1)


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_title(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/search_book')
        self.assertEqual(selenium.title, "Book Search")

    def test_search(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/search_book')
        # find the search input
        search_input = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'input-keyword')))
        search_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'search-button')))
        # fill the input
        search_input.send_keys("Gani")
        # click the search button
        search_button.click()
        # wait until result appeared
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'like')))
        # check result exist
        self.assertIn("Search Result", selenium.page_source)
        self.assertIn("Gani", selenium.page_source)

    def test_like_button(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/search_book')
        # find the search input
        search_input = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'input-keyword')))
        search_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'search-button')))
        # fill the input
        search_input.send_keys("Gani")
        # click the search button
        search_button.click()
        # wait until result appeared
        like_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'like')))
        count_like = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'count-like')))
        # Click the like button twice
        like_button.click()
        time.sleep(0.5)
        like_button.click()
        time.sleep(0.5)
        # The count_like should be 2 now
        self.assertEquals(count_like.get_attribute('innerHTML'),'2')
    
    def test_modal_button(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/search_book')
        # find the modal button
        modal_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-modal')))
        # Click at the first time 
        # The content of the modal should be empty (only a placeholder sentence)
        modal_button.click()
        time.sleep(1)
        self.assertIn("Discover more books and like your fav!", selenium.page_source)
        # Close the Modal
        close_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'close')))
        close_button.click()
        # Liking a book
        # find the search input
        search_input = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'input-keyword')))
        search_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'search-button')))
        # fill the input
        search_input.send_keys("Halo")
        # click the search button
        search_button.click()
        # wait until result appeared
        like_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'like')))
        count_like = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'count-like')))
        # Click the like button
        like_button.click()
        time.sleep(0.5)
        # Let's back to homepage
        selenium.get(self.live_server_url+'/search_book')
        # Click at the second time 
        # The content of the modal should contain book liked
        modal_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-modal')))
        modal_button.click()
        time.sleep(1)
        self.assertIn("Halo", selenium.page_source)

