from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('count_like/<str:book_id>', views.count_like, name="count_like"),
    path('top5/', views.get_top5, name="get_top5")
]