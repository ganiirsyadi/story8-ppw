from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class DataProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=250)
    photo = models.CharField(max_length=420)
    birthDate = models.DateField()
    city = models.CharField(max_length=100)
    def __str__(self):
        return self.name